;(function($){
    var swiper = new Swiper('.swiper-container', {
        slidesPerView: 1,
        spaceBetween: 0,
        loop: true,
        loopFillGroupWithBlank: true,
        draggable: true,
        navigation: {
          nextEl: '.button-next',
          prevEl: '.button-prev',
      },
        breakpoints: {

            400: {
                slidesPerView: 1

            },
            768: {
                slidesPerView: 1

            },
            992: {
                slidesPerView: 1

            }
        }
    });

})(jQuery);
